<?php

/**
 *
 * DeleteBuilder class
 *
 * Examples:
 * $db->delete('user');
 *
 */
class DeleteBuilder extends WhereBuilder
{

    private $delete;

    public function __construct($table)
    {
        $this->delete = 'DELETE FROM ' . trim($table);

        return $this;
    }

    /**
     * Get a SQL-request string.
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->delete . $this->where . ';';
    }

    /**
     * Get a SQL-request values.
     *
     * @return array
     */
    public function getQueryParams()
    {
        return $this->where_arr;
    }
}

?>